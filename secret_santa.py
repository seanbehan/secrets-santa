'''
CC0 2018

Author: Sean Behan
Name: Secret Santa
Desc: Secret Santa script
'''

import random
import json
import smtplib
import getpass


def read_data(filename):
    try:
        with open(filename) as stream:
            return json.load(stream)
    except OSError:
        return []


CONFIG = read_data('config.json')


def generate_message(pair):
    return '''\
Hello {name},

IT Secret Santa has begun! You have been matched with {pair}.
Please read their bio below and have a gift ready before {end_date}.

Name: {pair}
Likes: {likes}
Dislikes: {dislikes}
Allergies: {allergies}
Other: {other}

Ho ho ho and have a Merry Christmas!

-Santa Claus
'''.format(
        name=pair[0]['name'],
        pair=pair[1]['name'],
        end_date=CONFIG['end_date'],
        likes=pair[1]['likes'],
        dislikes=pair[1]['dislikes'],
        allergies=pair[1]['allergies'],
        other=pair[1]['other'])


def generate_pairs(data):
    random.shuffle(data)
    return list(zip(data, (2 * data)[1:]))


def send_email(pair):
    username = CONFIG['username']
    sender = CONFIG['sender']
    subject = CONFIG['subject']
    password = CONFIG['password']

    recipient = pair[0]['email']
    message = generate_message(pair)
    content = "\r\n".join([
        'From: {}'.format(sender), 'To: {}'.format(recipient),
        'Subject: {}'.format(subject), '', '{}'.format(message)
    ])
    try:
        server = smtplib.SMTP(CONFIG['server'])
        server.ehlo()
        server.starttls()
        server.login(username, password)
        server.sendmail(sender, recipient, content)
        return True
    # email sent successfully
    except OSError:
        return False
    # email failed to send


def read_name():
    out = {
        'name': input('Enter name: '),
        'email': input('Enter email address: '),
        'likes': input('Enter likes: '),
        'dislikes': input('Enter dislikes: '),
        'allergies': input('Enter allergies: '),
        'other': input('Enter other: ')
    }
    out['filename'] = out['name'].lower()
    return out


def add_name(name, data):
    data.append(name)
    with open('data.json', 'w') as stream:
        json.dump(data, stream)
    return data


def main():
    print('''\nWelcome to Secrets Santa!\n\nChoose an option:\n1. Add name to \
the data.json\n2. Generate Secret Santa Pairs\n3. Generate then send emails\n'''
          )

    options = [1, 2, 3]
    option = int(input())

    while option not in options:
        option = int(input())

    data = read_data('data.json')

    if option == 1:
        name = read_name()
        data = add_name(name, data)
    if option == 2:
        pairs = generate_pairs(data)
    if option == 3:
        pairs = generate_pairs(data)
        pass_message = 'Enter your SMTP password for {username}: '
        CONFIG['password'] = getpass.getpass(pass_message.format(username=CONFIG['username']))

        results = tuple(map(send_email, pairs))
        # send out emails and check for success or failure

        if False in results:
            print('Some emails failed to send')

if __name__ == '__main__':
    main()
